# neo4j-twitter

#### 介绍
使用neo4j图数据库展示twitter好友之间关注关系的的项目；

#### 软件架构
软件架构说明
1. twitter好友关系数据入库；
2. Local方式入库；
3. Bolt方式入库；
4. SDN（Spring Data Neo4j）；
5. 好友之间数据D3.js展示;

#### 使用说明
查看源代码

#### 参与贡献
1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

## 联系作者
- [oschina](http://git.oschina.net/liinux)
- [cnblogs](http://www.cnblogs.com/liinux)
- [github](https://github.com/liinnux)

## 欢迎关注微信公众号：
![关注微信二维码](http://wx1.sinaimg.cn/mw690/005ZM7sBgy1fy9ox7g2xzj3076076wey.jpg '关注微信二维码')

## 欢迎加入QQ交流群：
![加QQ群二维码](http://wx4.sinaimg.cn/mw690/005ZM7sBly1fsv9kqob23j308e08eaa0.jpg '加群二维码') 