
/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.neo4j.repository;

import java.util.List;

import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.repository.query.Param;

import com.tompai.neo4j.model.Node;

/**
* @desc: neo4j-tools
* @name: TwitterNeoRepository.java
* @author: tompai
* @createTime: 2019年1月2日 下午11:23:38
* @history:
* @version: v1.0
*/

public interface TwitterNeoRepository extends Neo4jRepository<Node,Long>{

    @Query("MATCH (n:name) where n.name='uyghurcongress' return n")
	List<Node> findByName(@Param("name")String name);
}

