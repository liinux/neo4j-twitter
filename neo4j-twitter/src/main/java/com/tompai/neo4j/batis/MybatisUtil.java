
/** 
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package com.tompai.neo4j.batis;

import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.ExecutorType;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
/**
* @desc: neo4j
* @name: MybatisUtil.java
* @author: tompai
* @createTime: 2018年12月30日 下午6:42:08
* @history:
* @version: v1.0
*/

public class MybatisUtil {
	
	private static SqlSessionFactory factory;  
    private static ThreadLocal<SqlSession> threadLocal=new ThreadLocal<SqlSession>();  
    private static String mybatisConfig="mybatis-config.xml";  
    
    /** 
     * 静态初始化 
     */ 
	/*static {
		try {
			// 使用MyBatis提供的Resources类加载mybatis的配置文件
			String resource = mybatisConfig;
			InputStream inputStream;
			inputStream = Resources.getResourceAsStream(resource);
			// 构建sqlSession的工厂
			factory = new SqlSessionFactoryBuilder().build(inputStream);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			// do nothing//
		}
	}*/
	
	private static void init() {
		try {
			// 使用MyBatis提供的Resources类加载mybatis的配置文件
			String resource = mybatisConfig;
			InputStream inputStream;
			inputStream = Resources.getResourceAsStream(resource);
			// 构建sqlSession的工厂
			factory = new SqlSessionFactoryBuilder().build(inputStream);
			//session = factory.openSession(ExecutorType.BATCH,false);
		} catch (Exception e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} finally {
			// do nothing//
		}
	}
	/**
	 * 禁止外界通过new方法创建 
	 */
	private MybatisUtil(){};
	/** 
     *  
    * @Title: getSession 
    * @Description: 该方法的主要作用：打开session 
    * @param  @return 设定文件   
    * @return  返回类型：SqlSession    
    * @throws 
     */  
    public static SqlSession getSession(){  
        SqlSession session=threadLocal.get();  
        if(session==null){  
        	init();
            session=factory.openSession(ExecutorType.BATCH,true);  
            threadLocal.set(session);  
        }  
        return session;  
    }  
      
    /** 
     *  
    * @Title: closeSession 
    * @Description: 该方法的主要作用：关闭session 
    * @param   设定文件   
    * @return  返回类型：void    
    * @throws 
     */  
    public static void closeSession(){  
        SqlSession session=threadLocal.get();  
        threadLocal.set(null);  
        if(session!=null){  
            session.close();  
            threadLocal.remove();
        }  
    } 
}