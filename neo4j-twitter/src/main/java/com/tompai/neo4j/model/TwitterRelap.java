package com.tompai.neo4j.model;

import java.util.Date;

public class TwitterRelap {
    private Long id;

    private String accounta;

    private String accountb;

    private String role;

    private Date inputTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccounta() {
        return accounta;
    }

    public void setAccounta(String accounta) {
        this.accounta = accounta == null ? null : accounta.trim();
    }

    public String getAccountb() {
        return accountb;
    }

    public void setAccountb(String accountb) {
        this.accountb = accountb == null ? null : accountb.trim();
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role == null ? null : role.trim();
    }

    public Date getInputTime() {
        return inputTime;
    }

    public void setInputTime(Date inputTime) {
        this.inputTime = inputTime;
    }
}